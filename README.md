# TEST AUTOMATION MODEL BLUEPRINT

Questo modello serve per la creazione base di un progetto di automazione basato sulla logica EPIC-USER STORY.
Creare una copia di questo modello con i comandi bare e mirror per avere una creazione pulita del nuovo progetto (che ovviamente deve essere creato in precedenza su bitbucket).

Questa applicazione si basa su *webdriverio* e *nodejs*. 
Come suite di test viene utilizzata *jasmine.js* (https://jasmine.github.io/).

### Settaggio del livello dei log in modalità "Silent"
```
npm config set loglevel "silent"
```

### Per utilizzare l'app occorre installare tutti i packages di node:
```
npm install
```

### Per eseguire una specifica *epic* di test (work in progress):
```
npm run build && npm run epic epic-0
```

## Per generare la reportistica relativa ai test eseguiti:
```
npm run report
```

## Installazione di allure 2 (framework per generazione della reportistica):
https://github.com/allure-framework/allure2

## Tip
Name convenction dei branch utilizzati:

* feature/user-story-1.01 : crea una nuova user story
* fix/cosa-sto-sistemando
* improvements/cosa-sto-migliorando

