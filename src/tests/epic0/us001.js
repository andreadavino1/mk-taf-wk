// login.spec.js
import LoginPage from '../pageObjects/login.page'

describe('login form', () => {
    it('should deny access with wrong creds', () => {
        LoginPage.open()
        LoginPage.username.setValue('foo')
        LoginPage.password.setValue('bar')
        LoginPage.submit()

        expect(LoginPage.flash).toHaveText('Your username is invalid!')
    })

    it('should allow access with correct creds', () => {
        LoginPage.open()
        LoginPage.username.setValue('tomsmith')
        LoginPage.password.setValue('SuperSecretPassword!')
        LoginPage.submit()

        expect(LoginPage.flash).toHaveText('You logged into a secure area!')
    })
})